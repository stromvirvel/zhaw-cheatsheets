\section{Anwendung der Ableitung}

\subsection{Geometrische Vorbetrachtungen}

\begin{itemize}
    \item Die 1. Ableitung beschreibt die Veränderung der Funktion $f$:
          \begin{itemize}
              \item $f'(x_0)>0$: $f$ steigt beim Durchgang durch den Punkt $P(x_0,f(x_0))$
              \item $f'(x_0)<0$: $f$ fällt beim Durchgang durch den Punkt $P(x_0,f(x_0))$
          \end{itemize}
    \item Die 2. Ableitung beschreibt das Krümmungsverhalten des Graphen:
          \begin{itemize}
              \item $f''(x_0)>0$: Die Steigung der Kurventangente nimmt beim Durchgang
                    durch den Punkt $P(x_0,f(x_0))$ zu; der Graph beschreibt eine \textbf{Linkskurve}.
              \item $f''(x_0)<0$: Die Steigung der Kurventangente nimmt beim Durchgang
                    durch den Punkt $P(x_0,f(x_0))$ ab; der Graph beschreibt eine \textbf{Rechtskurve}.
          \end{itemize}
    \item Für eine monoton wachsende, differenzierbare Funktion $f$ gilt: Ihre
          Steigung (also $f'(x)$) ist $\geq 0$. Umgekehrt gilt es auch. Analog zu monoton fallenden
          Funktionen.
          \begin{itemize}
              \item $f'(x)$ ist auf einem Intervall überall $\geq 0$ $\Leftrightarrow$
                    $f$ ist auf diesem Intervall \textbf{monoton steigend}.
              \item $f'(x)$ ist auf einem Intervall überall $\leq 0$ $\Leftrightarrow$
                    $f$ ist auf diesem Intervall \textbf{monoton fallend}.
          \end{itemize}
\end{itemize}

\subsection{Relative Extrema}
Eine Funktion $f$ besitzt an der Stelle $x_0$ ein \textbf{relatives
    Maximum}, wenn es eine Umgebung $U$ von $x_0$ gibt (benachbarte Punkte links
und rechts auf dem Graphen von $x_0$), sodass gilt: $f(x) \leq f(x_0)$ für
alle $x \in U$.

\begin{tabular}{ |m{6em}|m{12em}|m{12em}|m{12em}| }
    \hline
                         & $x_0$ heisst              & $f(x_0)$ heisst                             & $(x_0,y_0)$ heisst        \\
    \hline
    \textbf{Maximum}     & (relative) Maximalstelle  & (relatives) Maximum oder auch Maximalwert   & (relativer) Hochpunkt     \\
    \hline
    \textbf{Minimum}     & (relative) Minimalstelle  & (relatives) Minimum oder auch Minimalwert   & (relativer) Tiefpunkt     \\
    \hline
    \textbf{Oberbegriff} & (relative) Extremalstelle & (relatives) Extremum oder auch Extremalwert & (relativer) Extremalpunkt \\
    \hline
\end{tabular}

\begin{multicols}{2}
    \includegraphics[scale=0.25]{7/relative-extrema.png}

    \begin{itemize}
        \item Ein abgeschlossenes oder halboffenes Intervall besitzt \textbf{Randpunkte}:
              \begin{itemize}
                  \item $[a,b)$: Randpunkt $a$
                  \item $(a,b]$: Randpunkt $b$
                  \item $[a,b]$: Randpunkte $a, b$
              \end{itemize}

        \item Die Punkte eines Intervalls $I$, die keine Randpunkte sind, heissen innere
              Punkte von $I$.
    \end{itemize}
\end{multicols}


\begin{multicols}{2}

    \textbf{Kandidaten für relative Extrema}, sind:
    \begin{enumerate}
        \item Innere Punkte $x_0$ des Definitionsbereichs mit $f'(x_0)=0$.
        \item Randpunkte des Definitionsbereichs
    \end{enumerate}

    Nicht jedes $x_0$ mit $f'(x)=0$ ist eine Extremalstelle! Z.B. $f(x)=x^3$ ist
    $f'(0)=0$, aber $0$ ist keine Extremalstelle!

    \begin{tikzpicture}
        \begin{axis}[xmin=-4, xmax=4, ymin=-30, ymax=30, grid=both, width=0.5\textwidth, height=0.3\textwidth]
            \addplot[smooth, thick, blue] {x^3};
        \end{axis}
    \end{tikzpicture}

\end{multicols}


\begin{tabular}{ |m{10em}|m{12em}|m{12em}| }
    \hline
                               & \textbf{lokales Maximum}      & \textbf{lokales Minimum}      \\
    \hline
    \textbf{notwendige Bed.}   & $f'(x_0)=0$                   & $f'(x_0)=0$                   \\
    \hline
    \textbf{hinreichende Bed.} & $f'(x_0)$ wechselt von + zu - & $f'(x_0)$ wechselt von - zu + \\
    \hline
    \textbf{hinreichende Bed.} & $f''(x_0)<0$ (Rechtskurve)    & $f''(x_0)>0$ (Linkskurve)     \\
    \hline
\end{tabular}

\begin{itemize}
    \item \textbf{Wendepunkte} sind Punkte, an denen sich Krümmungsverhalten ändert
          (Linkskurve zu Rechtskurve oder umgekehrt). Bei einem Wendepunkt $x_0$ muss
          gelten $f''(x_0)=0$ und $f'''(x_0) \ne 0$.
    \item \textbf{Sattelpunkte} sind Wendepunkte mit horizontaler Tangente, also
          muss gelten $f'(x_0)=0$.
\end{itemize}

\subsection{Kurvendiskussion}

Man stellt sich Überlegungen an, um sich ein recht gutes Bild vom Verlauf des
Graphen einer Funktion zu machen. Dazu stellt man sich einige Fragen, um die
notwendige Informationen zu sammeln. Nicht alle Fragen sind immer zwingend zu
beantworten (adaptieren nach Bedarf). Das Resultat einer Kurvendiskussion ist
immer eine Skizze des Graphen.

\subsubsection{Fragenkatalog für die Kurvendiskussion}

\begin{enumerate}
    \item Definitionsbereich?
    \item Symmetrieeigenschaften (gerade/ungerade), Periode?
    \item Schnittpunkte mit Achsen, Polstellen?
    \item Randpunkte bzw. Verhalten, wenn $x$ gegen die (äusseren?) Grenzen des
          Definitionsbereichs strebt?
    \item Kandidaten für Extrema bestimmen und untersuchen
    \item Wendepunkte suchen
    \item Tabelle von Werten aufstellen (falls noch nötig)
\end{enumerate}

%\item Extrempunkt ($f'(x_0)=0$) (TODO maybe delete)

\subsubsection{Extremwertaufgaben/Optimierungsprobleme}

Viele Extremwertaufgaben sind Optimierungsprobleme. Oft geht es darum, eine
Grösse (Zeit, Geld, Erfolg) unter gegebenen Nebenbedingungen (Anzahl
Mitarbeiter, Arbeitszeiten, Gesetze) zu optimieren. Es geht darum, ein
\textbf{absolutes Maximum oder Minimum} zu finden. Es gibt jedoch Funktionen,
die weder ein absolutes Maximum noch ein absolutes Minimum besitzen.


\subsubsection{Hilfreiche Schritte beim Lösen von Extremwertaufgaben}

\begin{enumerate}
    \item Zielgrösse (Funktionswert) identifizieren (z.B. Höhe).
    \item Unabhängige Variable (Funktions-Input) identifizieren (z.B. Zeit).
    \item Definitionsbereich bestimmen.
    \item Zielgrösse als Funktion der unabhängigen Variablen ausdrücken; ev. eine qualitative Skizze des Graphen machen.
    \item Relative Maxima resp. Minima bestimmen; Randpunkte auch
          berücksichtigen!
    \item Untersuchen, welche der relativen Extrema auch absolute Extrema sind (inklusive – bei
          offenen und halboffenen Intervallen – Betrachtung der Funktion in der
          Nähe des Randes $\lim_{x \rightarrow a}f(x)$ und $\lim_{x \rightarrow b}f(x)$)
    \item Die gesuchte Information aus den Berechnungen extrahieren.
          (Ev. nachschauen, nach welcher Grösse gefragt wurde: Extremalstelle? Extremalwert? Extremalpunkt?)
\end{enumerate}
