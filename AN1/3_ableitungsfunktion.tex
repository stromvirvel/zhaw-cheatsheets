\section{Ableiten (Differenzieren) von Funktionen}

Ableiten ist ein mathematischer Begriff, mit dem man viele Konzepte aus Physik,
Wirtschaft, etc (z.B. Geschwindigkeit, Zuwachsrate) mathematisch fassen kann.
Die Ableitung einer Funktion an einer bestimmten Stelle gibt Auskunft über die
Entwicklung/Veränderung dieser Funktion. Sie erlaubt uns, die Gerade, die einer
Funktion in der Umgebung einer bestimmten Stelle am nächsten kommt, zu bestimmen
(lineare Approximation). \textbf{Seite 130}.

\subsection{Idee}
\begin{itemize}
    \item Es sei ein Läufer der einen Marathon läuft, und über die ganze Strecke
          immer etwas langsamer wird ($s(t)$). Um aus dem Graphen von einem
          bestimmten Zeitpunkt
          $t_0$ die Momentangeschwindigkeit zu berechnen, bestimmt man ein
          kleines $\Delta t$, um die Momentangeschwindigkeit zwischen $t_0$ und
          $t_0+\Delta t$ zu berechnen: $\frac{s(t_0+\Delta t)-s(t_0)}{\Delta
                  t}$. Erinnere, die Geschwindigkeitsformel: $\frac{\Delta s}{\Delta
                  t}$.
    \item Verallgemeinert lässt sich vom obigen Beispiel die
          Momentangeschwindigkeit als \textit{Differentenquotenten} bezeichnen:
          $\frac{f(x_0+\Delta x)-f(x_0)}{\Delta x}$.
    \item Die erste Ableitung $f'(x_0)$ gibt den Wert des Differentenquotenten
          an, wenn $\Delta x$ gegen $0$ strebt (oder auch: die Steigung der
          Tangente).
    \item Schreibweisen: $f'(x)$, $\frac{df}{dx}$, $\frac{dy}{dx}$ ($d$
          steht für Derivative).
    \item \textbf{Satz}: Eine konstante Funktion ($f(x) = c$), dann ist immer $f'(x) =
              0$, weil die Steigung einer horizontalen Geraden überall $0$ ist.
    \item \textbf{Satz}: Eine Funktion $f(x) = x^k$ mit $k \neq 0$,
          dann gilt $f'(x) = k \cdot x^{k-1}$.
    \item Die \textit{zweite Ableitung} $f''(x)$ erhält man durch Ableiten von $f'(x)$.
    \item Die \textit{dritte Ableitung} $f'''(x)$ erhält man durch Ableiten von $f''(x)$.
\end{itemize}

\subsection{Ableitungsregeln (Seite 133)}

\begin{itemize}
    \item
          Faktorregel:
          \begin{equation}
              (c \cdot f)'(x) = c \cdot f'(x)
          \end{equation}

    \item
          Summenregel:
          \begin{equation}
              (f+g)'(x) = f'(x) + g'(x)
          \end{equation}

    \item
          Produktregel:
          \begin{equation}
              (u \cdot v)'(x) = u'(x) \cdot v(x) + u(x) \cdot v'(x)
          \end{equation}

    \item
          Quotientenregel:
          \begin{equation}
              (\frac{u}{v})' (x) = \frac{u'(x) \cdot v(x) - u(x) \cdot v'(x)}{(v(x))^2}
          \end{equation}

    \item
          Kettenregel:
          \begin{equation}
              (F \circ u)'(x) = F'(u) \cdot u'(x)
          \end{equation}
          Wobei die äussere Funktion: $F(u)$ und die innere Funktion $u(x)$ ist.
          Falls im Ergebnis noch ein $u$ vorkommt, diese noch mit der Funktion
          $u(x)$ ersetzen. \textbf{Wichtig}: Wirklich $F(u)$ schreiben, nicht
          $F(x)$, weil $x$ und $u$ unterschiedliche Variablen sind!
\end{itemize}


\subsection{Ableitungen bestimmer Funktionen}
$x$ ist der Parameter, nach dem abgeleitet wird. $a$ eine beliebige Konstante:
\begin{itemize}
    \item $(sin(x))' = cos(x)$
    \item $(cos(x))' = -sin(x)$
    \item $(e^x)' = e^x$
    \item $(a^x)' = a^x \cdot ln(a)$
    \item $(ln(x))' = \frac{1}{x}$
    \item $(log_a(x))' = \frac{1}{x \cdot ln(a)}$
\end{itemize}

\subsection{Differenzierbarkeit}

Es gibt Funktionen, die an bestimmten Stellen \textbf{keine} Ableitung haben!
Sie ist also am Punkt $x_0$ \textit{nicht differenzierbar}. Grundsätzlich ist
eine Funktion an einer Stelle $x_0$ nicht differenzierbar, wenn sie an der
Stelle $x_0$ einen ``Knick'' hat. Beispiel Minimalfunktion $f(x) = min(x^2,5)$:


\begin{tikzpicture}
    \begin{axis}[xmin=-4, xmax=4, ymin=-10, ymax=10, grid=both, width=0.5\textwidth, height=0.3\textwidth]
        \addplot[thick, blue] {min(x^2,5)};
    \end{axis}
\end{tikzpicture}

\textbf{Formale Definition}: Eine Funktion $f(x)$ ist an der Stelle $x_0$
\textit{differenzierbar}, wenn die linksseitige mit der rechtsseitigen Ableitung
übereinstimmt.

Falls eine Funktion an jeder Stelle ihres Definitionsbereichs eine Ableitung
hat, heisst die Funktion \textit{differenzierbar}.

Fortführung Beispiel $f(x) = min(x^2,5)$:

\begin{itemize}
    \item An der Stelle $x_0 = - \sqrt{5}$
          \begin{itemize}
              \item linksseitige Funktion: $f(x) = 5$
              \item \textbf{linksseitige Ableitung}: $f'_{links}(x_0) = 0$
              \item rechtsseitige Funktion: $f(x) = x^2$
              \item \textbf{rechtsseitige Ableitung}: $f'_{rechts}(x_0) = 2x = -2 \cdot \sqrt{5}$
              \item Ist an der Stelle $x_0 = - \sqrt{5}$ \textit{nicht} differenzierbar, weil
                    $0 \neq -2 \cdot \sqrt{5}$
          \end{itemize}
    \item An der Stelle $x_0 = \sqrt{5}$
          \begin{itemize}
              \item linksseitige Funktion: $f(x) = x^2$
              \item \textbf{linksseitige Ableitung}: $f'_{links}(x_0) = 2 \cdot \sqrt{5}$
              \item rechtsseitige Funktion: $f(x) = 5$
              \item \textbf{rechtsseitige Ableitung}: $f'_{rechts}(x_0) = 0$
              \item Ist an der Stelle $x_0 = \sqrt{5}$ \textit{nicht} differenzierbar, weil
                    $\cdot \sqrt{5} \neq 0$
          \end{itemize}
\end{itemize}

\subsection{Linearisierung einer Funktion}

\begin{itemize}
    \item Die erste Ableitung der Funktion $f(x)$ durch den Punkt $x_0$ ist die
          Tangente (lineare Funktion) an diese Funktion durch diesen Punkt.
    \item Nicht vergessen, lineare Funktion: $y = m \cdot x + b$
    \item \textbf{Die Funktionsgleichung für die Tangente} von $f(x)$ an der
          Stelle $x_0$ lautet:
          \begin{equation}
              f(x) = y = f'(x_0) \cdot (x - x_0) + f(x_0)
          \end{equation}
\end{itemize}

\subsection{Numerik: Newton-Verfahren (Seite 24) TODO}

Für trigonometrische Funktionen, oder Polynome mit Grad $\geq 5$ gibt es keine
Formel, um die Lösung exakt zu berechnen. Deshalb werden Verfahren verwendet,
welche einen \textit{Näherungswert} für die Lösung liefern. Das
\textit{Newtonverfahren} beruht darauf, dass die Tangente in vielen Fällen eine
gute Näherung der Funktion darstellt.

\subsection{Ableitungsfunktion Skizzieren}

Um die Ableitungsfunktion zu skizzieren, folgendes beachten:

\begin{itemize}
    \item Die Nullstellen der Ableitung $f'(x)$ ist dort, wo die Tangente der
          Funktion $f(x)$ horizontal ist.
    \item Der Grad der Ableitung ist eins kleiner als der seiner ursprünglichen Funktion.
    \item Graphenverlauf anhand des Grades bestimmen: Ist der Grad
          ungerade, startet der Graph unten links, ist er gerade,
          startet der Graph oben rechts.
    \item Startet/endet jedoch die Funktion an der x-Achse, so startet/endet dessen
          Ableitung auch auf der x-Achse.
\end{itemize}

\includegraphics[scale=0.6]{3/ableitung-skizzieren.png}

\subsection{Lösungstipps}

\begin{itemize}
    \item Wenn es heisst, bestimmen Sie eine quadratische Funktion, und es sind
          weitere Infos gegeben wie Steigung bei Punkt $x_1$ und Steigung bei Punkt
          $x_2$, dann ist mit Steigung die erste Ableitung gemeint. Gelöst wird es mit
          der Grundform der quadratischen Gleichung: $f(x) = ax^2 + bx + c$ und dessen
          Ableitung $f'(x) = 2ax + b$.
    \item Wenn es heisst, man soll etwas bestimmen mit ``waagrechter Tangente'',
          dann ist gemeint, dass die Ableitung (Steigung) $0$ ist.
\end{itemize}