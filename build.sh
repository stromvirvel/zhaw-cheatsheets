#!/usr/bin/env bash

mkdir public
base=$(pwd)

# generate PDF files
for file in `find . -name "main.tex"`;
do
    cd "$PWD/$(dirname ${file})"
    latexmk -pdf $(basename ${file})
    cd "$base"
done


# move PDF files to public folder
for file in `find . -name "main.pdf"`;
do
    cd "$PWD/$(dirname ${file})"
    mv $(basename ${file}) ../public/$(dirname ${file}).pdf
    cd "$base"
done


# generate index.html in public folder
cat<<EOF > public/index.html
<html>
<head>
    <title>ZHAW cheat sheets</title>
    <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css" integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/base-min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
    /*
    When setting the primary font stack, apply it to the Pure grid units along
    with "html", "button", "input", "select", and "textarea". Pure Grids use
    specific font stacks to ensure the greatest OS/browser compatibility.
    */
    html, button, input, select, textarea,
    .pure-g [class *= "pure-u"] {
        /* Set your content font stack here: */
        font-family: Georgia, Times, "Times New Roman", serif;
    }
    </style>
</head>
<body>
    <div class="pure-g">
    <div class="pure-u-2-24">
    </div>
    <div class="pure-u-20-24">
    <h1>ZHAW cheat sheets</h1>
    
    <h2>Overview</h2>
    This page lists cheat sheets for some modules at ZHAW computer science bachelor.

    <h2>Contents</h2>
    <p>
        <ul>
EOF


for file in `find ./public -name "*.pdf"`;
do
    echo "<li><a href=\"$(basename ${file})\">$(basename ${file})</a></li>"  >> public/index.html
done


cat<<EOF >> public/index.html
        </ul>
    </p>
    <h2>Contribute</h2>
    <p>
    The contents of the cheat sheets are managed in a
    <a href="https://gitlab.com/stromvirvel/zhaw-cheatsheets">git repository</a>.
    If you want to contribute, don't hesitate to open a
    <a href="https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html">merge request</a>.
    </p>

    <h2>Contact</h2>
    <p>
    You can contact me at:
    <ul>
        <li><a href="https://t.me/stromvirvel">Telegram</a></li>
        <li><a href="https://threema.id/CXRE29DC">Threema</a></li>
        <li><a href="https://keybase.io/stromvirvel">Keybase</a></li>
        <li><a href="mailto:wengere1@students.zhaw.ch">university mail address</a></li>
    </ul>
    </p>
</div>
</div>
</body>
</html>
EOF