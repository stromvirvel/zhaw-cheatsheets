\section{Faltungscodes}

\subsection{Motivation}

Blockcodes sind sehr effizient, um 1 Bitfehler zu korrigieren.
In der Praxis treten oft mehr als 1 Fehler auf (Burst-Fehler).
Für Mehrbitfehler steigt, der Aufwand der Blockcodes exponentiell.
Zur Korrektur von Mehrbitfehlern werden darum andere Codes verwendet:
Zum Beispiel \textbf{Faltungscodes}:

\begin{multicols}{2}
    \begin{itemize}
        \item Daten sind wie auf einem Film ``aufgewickelt''. Der wird abgerollt
        und durch ein ``Fenster'' geschoben.

        \includegraphics[scale=0.5]{13/film-abrollen-fenster.png}    
        \item Die Codebits werden aus dem Bitmuster im Fenster ermittelt.
        \item Mathematisch wird dieser Vorgang als Faltung beschrieben.
        \item Faltungscodes sind in der Regel \textit{nicht systematisch}, also die u-Bits
        kann man nicht einfach rauslesen, sondern müssen berechnet werden.
        \item Der Encoder lässt sich sehr leicht und preiswert in Hardware
        realisieren. Der Decoder benötigt etwas mehr Aufwand.
    \end{itemize}
\end{multicols}

\subsection{Hardwareimplementierung Encoder}

\begin{multicols}{2}
    \includegraphics[scale=0.2]{13/encoder-hardware.png}

    \begin{itemize}
        \item Linearer Code (da lineare Schaltung)
        \item $m$ ist die Anzahl an Flipflops (Gedächnislänge).
        \item Einflusslänge auf $c_{2k} = m + 1$ ($u_k, u_{k-1}, u_{k-2}$)
        \item Pro Datenbit $u_k$ erzeugt der Encoder immer zwei Codebits
        $c_{2k}$ und $c_{2k+1}$ (egal wieviel $m$)
        \item $N$ berechnen: Für jedes Eingangbit $K$ kommen zwei Codebits raus,
        plus noch $m$ Tailbits: $N = 2K + 2m$
        \item $c_{2k} = u_{k} \oplus u_{k-1} \oplus u_{k-2}$
        \item $c_{2k+1} = u_{k} \oplus u_{k-2}$
    \end{itemize}
\end{multicols}

\includegraphics[scale=0.3]{13/table-input-output.png}

\subsection{Zustandsdiagramm und freie Distanz}

\includegraphics[scale=0.3]{13/zustandsdiagram-beispiel.png}

\begin{multicols}{2}
    Bei Faltungscodes spricht man nicht von minimaler Hamming-Distanz,
    sondern einer \textbf{freien Distanz} $d_{free}$. Beantwortet die Fragen:
    \begin{itemize}
        \item Wie viele Bits muss ich drehen bis ich beim nächsten gültigen Code bin?
        \item Wieviele Fehler kann ich erkennen oder korrigieren?
    \end{itemize}
    Da Faltungscodes stets linear sind, gilt auch $d_{free} = w_{min}$

    \textit{Wie Code konstruieren, mit möglichst wenigen Einsen, aber mind. eine Eins?} Wir
    starten bei $00$ und müssen wieder bei $00$ enden. Resultat: $c = (11'10'11)$
    ergibt ein $d_{free} = w_{H}(111011) = 5$. 

    \textbf{Regel}: Es können $\lfloor \frac{d_{free}-1}{2} \rfloor$ Fehler korrigiert
    werden in einem Rahmen von $N = 3 \cdot m \dots 6 \cdot m$ Bits (weil wir ja bei
    Faltungscode einen ``Stream'' haben, und keinen ``Block''). Also $k = 2$
    Bitfehler werden im Range von 6 bis 12 Bits erkannt (weil $m = 2$).
\end{multicols}

\subsection{Optimum Free Distance (OFD)}

\begin{multicols}{2}
    \begin{itemize}
        \item ``Bauvorlagen'' für Encoder, die als Polynome beschrieben sind.
        \item $\gamma$ ist die Anzahl Generatoren, $m$ die Länge des
        Schieberegisters. Sie bestimmen die Qualität des Codes.
        \item $\gamma=3$, erlaubt bessere Fehlererkennung als $\gamma=2$.
        \item Zu jedem Wertepaar $(\gamma ; m)$ gibt es eine maximal mögliche $d_{free}$.
        \item Codes, die $d_{free}$ erreichen, nennt man OFD-Codes.
    \end{itemize}

    \includegraphics[scale=0.2]{13/optimum-free-distance.png}
\end{multicols}

\subsubsection{Vom Gewichtungsvektor zur Schaltung}

Beispiel: $(1101_b, 1111_b)$: Oben die erste Zahl, unten die zweite Zahl. Bei
jeder Ziffer kommt ein XOR-Tor ($\oplus$). Dazwischen ein Flipflop, die
miteinander verbunden sind. Jedes XOR-Tor das eine Eins repräsentiert, wird mit
der Leitung verbunden. In die ersten beiden XOR-Tore (vertikal) ist der Input
$0$. Der Input in das erste Flipflop ist $u_k$. Der Output oben rechts ist
$c_{2k}$ und unten rechts $c_{2k+1}$.

\includegraphics[scale=0.3]{13/schema-hw-encoder.png}

Optimiert: Alle XOR-Tore die eine $0$ als Eingang haben, werden weggelassen:

\includegraphics[scale=0.3]{13/schema-hw-encoder-optimiert.png}

\textbf{Impulsantwort der Generatoren}: Wird am Eingang eine Eins,
gefolgt von $m$ Nullen angelegt, erscheinen am Ausgang die Gewichtungsvektoren.

\subsection{Mathematische Beschreibung}

\includegraphics[scale=0.2]{13/mathematischer-output-1.png}

\includegraphics[scale=0.25]{13/mathematischer-output-2.png}

\subsection{Trellis-Diagramm}

Trellis-Diagramm ist ein zeitlich versetztes Zustandsdiagramm, wobei die
Zustände mehrfach (pro Zeitabschnitt) eingezeichnet sind. Beispiel
Trellis-Diagramm mit $m=2$:

\includegraphics[scale=0.3]{13/trellis-diagramm.png}

Codeworte bilden mithilfe Trellis-Diagram. Beispiel Input $u_k = (110100)$
(siehe Pfad in gelb). Man startet beim Zustand $(00)$ unten links, folgt dem
ersten u-Bit ($1$), erhält dabei den {\color{Green}{Output $11$}}. Wiederholen
bis keine u-Bits mehr kommen. Alle Outputs gesammelt ergibt es das
{\color{Green}{Codewort $c = (11'01'01'00'10'11)$}}.

\subsection{Viterbi-Decoder}
\begin{itemize}
    \item Mit wievielen minimalen Kosten kann ich die Zustände erreichen. Alle
    Pfade, die mehr Kosten (Fehlermetriken), werden gestrichen.
    \item Wenn alle Pfade evaluiert wurden, wird beim Zustand $(00)$ unten
    rechts der Pfad rückwärts zu\-rück\-ver\-folgt (eindeutig möglich - es gibt nicht
    mehrere Pfade).
\end{itemize}

