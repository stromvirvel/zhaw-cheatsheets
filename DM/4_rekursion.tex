\section{Rekursive Strukturen und die natürlichen Zahlen}


\subsection{Induktionsbeweis}
Wir haben eine Behauptung (Prädikat), bzw. die Induktionsannahme (IA) $A(n)$. Also zum Beispiel:

\begin{equation}
    A(n): \sum_{k=1}^{n} k^3 = \frac{n^2 \cdot (n+1)^2}{4}, \ n \in \mathbb{N}, n \geq 1
\end{equation}

Der Induktionsbeweis wird nun wie folgt geführt:

\begin{enumerate}
    \item Induktionsverankerung (IV) heisst, wir
    nehmen das kleinste $n$, und beweisen, dass $A(n)$ gilt
    (\textbf{muss immer} gemacht werden, sonst ist Beweis nicht gültig).
    \begin{equation}
        \begin{aligned}
            \sum_{k=1}^{1} k^3 = 1^3 = 1 \\
            \frac{1^2 \cdot 2^2}{4} = 1
        \end{aligned}
    \end{equation}

    \item Induktionsbehauptung (IB), also für die nächstgrössere Zahl: Annahme
    nehmen aber $n+1$ einsetzen anstatt $n$.
    \begin{equation}
        \sum_{k=1}^{n+1} k^3 = \frac{(n+1)^2 \cdot (n+2)^2}{4}
    \end{equation}

    \item Induktionsschritt (IS):
    \begin{itemize}
        \item Für den Induktionsschritt müssen wir zeigen, dass für jede
        natürliche Zahl $n$ mit der Eigenschaft $A(n)$ auch $A(n + 1)$ gilt.
        \item Die IB aufteilen in linker und rechter Term, diese separat anschauen.
        \item Nun versuchen, den Term von der Induktionsannahme abspalten.
        \item Also wir nehmen den linken Term der IB, setzen den gleich den
        linken Term der Induktionsannahme, und finden heraus was bei der
        Induktionsannahme noch fehlt um auf den IB zu kommen:
        \begin{equation}
            \sum_{k=1}^{n+1} k^3 = \sum_{k=1}^{n} k^3 + (n+1)^3
        \end{equation} 
        \item Also hier mussten wir $(n+1)^3$ anhängen, damit die Gleichung stimmt.
        \item Nun nehmen wir diese Gleichung, ersetzen aber den linken IB-Term
        mit dem rechten IB-Term, und den linken IA-Term mit dem rechten IA-Term.
        Dann sieht die Gleichung so aus:
        \begin{equation}
            \frac{(n+1)^2 \cdot (n+2)^2}{4} = \frac{n^2 \cdot (n+1)^2}{4} + (n+1)^3
        \end{equation}
        \item Nun formen wir die Gleichung solange um, bis der linke und der
        rechte Term gleich sind. Wenn wir das schaffen, haben wir die Behauptung bewiesen.
        \begin{equation}
            \begin{aligned}
                (n+1)^2 \cdot (n+2)^2               & = n^2 \cdot (n+1)^2 + 4 \cdot (n+1)^3 \\
                (n+2)^2                             & = n^2 + 4 \cdot (n+1) \\
                (n+2)^2                             & = n^2 + 4n + 4 \\
                (n+2)^2                             & = (n+2)^2 \\
            \end{aligned}
        \end{equation}
    \end{itemize}
\end{enumerate}

\subsection{Die grundlegende Struktur der natürlichen Zahlen}

\begin{multicols}{2}

\begin{itemize}
    \item Satz 17: Für jede Menge X von natürlichen Zahlen gilt: Wenn X die
    folgenden Bedigungen erfüllt, dann ist bereits $X = \mathbb{N}$
    \begin{itemize}
        \item Induktionsverankerung: $0 \in X$
        \item Induktionsschritt $\forall n (n \in X \Rightarrow n + 1 \in X)$
    \end{itemize}
    \item Satz 18: Jede nichtleere Menge von natürlichen Zahlen hat ein
    minimales Element.
    \item Satz 19: Es gibt keine unendlich absteigende Folge von natürlichen
    Zahlen $a_0 > a_1 > \dots > a_n > a_{n+1}$. Gäbe es diese absteigende Folge,
    dann hätte die Menge kein minimales Element. Dies widerspricht Satz 18.
\end{itemize}

Mit diesen bewiesenen Sätzen können wir eine neue Beweismethode herleiten.
\textbf{Der kleinste Verbrecher}:
Man will zeigen, dass alle natürlichen Zahlen eine Eigenschaft $E$
haben. Wenn dies nicht der Fall wäre, gäbe es eine kleinste natürliche Zahl
$n_0$ (den kleinsten Verbrecher), die \textit{nicht} die Eigenschaft $E$
hat. Führt man diese Annahme zu einem Widerspruch, so hat man die ursprüngliche
Behauptung bewiesen. Die Methode ist daher eine Kombination aus
Widerspruchsbeweis und Satz 18.

\textbf{Beispiel}: Beweisen, dass jede (natürliche) Zahl von der Form ($n^2+n$)
gerade ist.

Wir nehmen an, dass es ungerade natürliche Zahlen von der Form $n^2+n$ gibt. Die
Zahl $n^2+n$ sei die kleinste solche Zahl (der kleinste Verbrecher). Weil $n$
nicht Null sein kann (sonst wäre $n^2+n$ gerade), muss es ein $k\in \mathbb{N} $ mit
$n=k+1$ geben. Weil $k<n$ gilt, muss $k^2+k$ aber gerade sein. Daraus folgt 

\begin{align*}
    n^2+n &= (k+1)^2+(k+1)= k^2 + 2k + 1 + k + 1\\
    &= \underbrace{k^2+k}_{gerade}+\underbrace{2k+2}_{gerade}
\end{align*}
und somit, dass $n^2+n$ gerade ist (im Widerspruch zur Annahme).

\end{multicols}

\subsection{Rekursive Definition}

Rekursive Definitionen bezeichnen die mathematisch einwandfreie Art, ein Objekt
durch Bezugnahme (Selbstreferenz) auf das zu definierende Objekt selbst zu
definieren.

\textbf{Beispiel}: Ein Palindrom ist ein Wort, das rückwärts und vorwärts
gelesen dasselbe Wort ergibt. Mathematisch betrachtet: Ein Wort $w$ ist ein
Palindrom, wenn mindestens einer der folgenden Bedingungen erfüllt ist:

\begin{itemize}
    \item Das Wort $w$ besteht aus einem oder gar keinem Buchstaben.
    \item Es gibt einen Buchstaben $x$, und ein
    $\underbrace{Palindrom}_{Selbstreferenz}$ $u$ so, dass $w=xux$ gilt.
\end{itemize}