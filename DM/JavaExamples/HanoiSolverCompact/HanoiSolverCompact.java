package DM.JavaExamples.HanoiSolverCompact;

public class HanoiSolverCompact {
    public static final void main(String[] args) {
        System.out.println((new HanoiSolverCompact()).solve(3));
    }

    public String solve(int size) {
        return hanoi("A", "C", "B", size);
    }

    private String hanoi(String x, String y, String z, int n) {
        if (n == 0)
            return "";
        return hanoi(x, z, y, n - 1) + " " + x + y + hanoi(z, y, x, n - 1);
    }
}
