package DM.JavaExamples.HanoiSolver;

public class HanoiSolver {

    public static final void main(String[] args) {
        System.out.println((new HanoiSolver()).solve(3));
    }

    public String solve(int size) {
        return AC(size);
    }

    private String AB(int x) {
        if (x == 0)
            return "";
        return AC(x - 1) + " AB " + CB(x - 1);
    }

    private String AC(int x) {
        if (x == 0)
            return "";
        return AB(x - 1) + " AC " + BC(x - 1);
    }

    private String BC(int x) {
        if (x == 0)
            return "";
        return BA(x - 1) + " BC " + AC(x - 1);
    }

    private String BA(int x) {
        if (x == 0)
            return "";
        return BC(x - 1) + " BA " + CA(x - 1);
    }

    private String CB(int x) {
        if (x == 0)
            return "";
        return CA(x - 1) + " CB " + AB(x - 1);
    }

    private String CA(int x) {
        if (x == 0)
            return "";
        return CB(x - 1) + " CA " + BA(x - 1);
    }
}
