package DM.JavaExamples.Uebung34;

public class Uebung34 {
    public static void main(String[] args) {
        System.out.println(Uebung34.addieren(8, 5));
    }

    static int addieren(int x, int y) {
        if (y == 0) {
            return x;
        }
        return addieren(x + 1, y - 1);
    }
}
