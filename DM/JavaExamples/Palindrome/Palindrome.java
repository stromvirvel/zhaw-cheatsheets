package DM.JavaExamples.Palindrome;

public class Palindrome {
    public static void main(String[] args) {
        String myPali = "sugus";
        if (Palindrome.palindrome(myPali)) {
            System.out.println("Is a Palindrome!");
        } else {
            System.out.println("Is NOT a Palindrome!");
        }
    }

    static boolean palindrome(String w) {
        if (w.length() < 2) {
            return true;
        }

        int last = w.length() - 1;
        char a = w.charAt(0);
        char b = w.charAt(last);

        System.out.println("First char: " + a);
        System.out.println("Last char: " + b);

        if (a == b) {
            return palindrome(w.substring(1, last));
        }
        return false;
    }
}
